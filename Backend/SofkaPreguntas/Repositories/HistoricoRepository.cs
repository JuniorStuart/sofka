﻿using Microsoft.Extensions.Configuration;
using SofkaPreguntas.Models;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SofkaPreguntas.Repositories
{
    public class HistoricoRepository
    {
        private readonly IConfiguration _configuration;

        public HistoricoRepository(IConfiguration config)
        {
            _configuration = config;
        }

        public IEnumerable<Historico> ObtenerHistorico()
        {
            var historico = new List<Historico>();
            var cnx = new SqlConnection(_configuration.GetConnectionString("PreguntasDatabase"));
            var cmd = new SqlCommand("ObtenerHistorico", cnx);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cnx.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                historico.Add(new Historico
                {
                    Nombre = reader["Nombre"].ToString(),
                    Puntos = reader.GetInt32(reader.GetOrdinal("Puntos"))
                });
            }
            cnx.Close();
            return historico;
        }

        public void GuardarResultado(Historico historico)
        {
            var cnx = new SqlConnection(_configuration.GetConnectionString("PreguntasDatabase"));
            var cmd = new SqlCommand("GuardarResultado", cnx);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre", historico.Nombre);
            cmd.Parameters.AddWithValue("@puntos", historico.Puntos);
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();
        }
    }
}
