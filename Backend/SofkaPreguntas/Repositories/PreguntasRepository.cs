﻿using Microsoft.Extensions.Configuration;
using SofkaPreguntas.Models;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SofkaPreguntas.Repositories
{
    public class PreguntasRepository
    {
        private readonly IConfiguration _configuration;

        public PreguntasRepository(IConfiguration config)
        {
            _configuration = config;
        }

        public IEnumerable<Pregunta> PreguntasPorNivel(int nivel)
        {
            var preguntas = new List<Pregunta>();
            var cnx = new SqlConnection(_configuration.GetConnectionString("PreguntasDatabase"));
            var cmd = new SqlCommand("PreguntasPorCategoria", cnx);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Nivel", nivel);
            cnx.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                preguntas.Add(new Pregunta
                {
                    Id = reader.GetInt32(reader.GetOrdinal("Id")),
                    Descripcion = reader["Descripcion"].ToString(),
                    Categoria = reader["Categoria"].ToString(),
                    Nivel = reader.GetInt32(reader.GetOrdinal("Nivel")),
                    Puntos = reader.GetInt32(reader.GetOrdinal("Puntos"))
                });
            }
            cnx.Close();
            return preguntas;
        }

        public IEnumerable<Respuesta> RespuestasPorPregunta(int preguntaId)
        {
            var respuestas = new List<Respuesta>();
            var cnx = new SqlConnection(_configuration.GetConnectionString("PreguntasDatabase"));
            var cmd = new SqlCommand("RespuestasPorPregunta", cnx);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", preguntaId);
            cnx.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                respuestas.Add(new Respuesta
                {
                    Descripcion = reader["Descripcion"].ToString(),
                    EsCorrecta = reader.GetBoolean(reader.GetOrdinal("EsCorrecta"))
                });
            }
            cnx.Close();
            return respuestas;
        }
    }
}
