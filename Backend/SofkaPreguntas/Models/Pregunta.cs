﻿using System.Collections.Generic;

namespace SofkaPreguntas.Models
{
    public class Pregunta
    {
        public int Id { get; set; }
        public string Categoria { get; set; }
        public string Descripcion { get; set; }
        public int Nivel { get; set; }
        public int Puntos { get; set; }

        public IEnumerable<Respuesta> Respuestas { get; set; }
    }
}
