﻿namespace SofkaPreguntas.Models
{
    public class Respuesta
    {
        public string Descripcion { get; set; }
        public bool EsCorrecta { get; set; }
    }
}
