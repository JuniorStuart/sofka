﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SofkaPreguntas.Models
{
    public class Historico
    {
        public string Nombre { get; set; }
        public int Puntos { get; set; }
    }
}
