﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SofkaPreguntas.Managers;
using SofkaPreguntas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SofkaPreguntas.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HistoricoController : ControllerBase
    {
        private readonly HistoricoManager _historicoManager;
        public HistoricoController(HistoricoManager historicoManager)
        {
            _historicoManager = historicoManager;
        }

        [HttpGet()]
        public IEnumerable<Historico> Get()
        {
            return _historicoManager.ObtenerHistorico();
        }

        [HttpPost()]
        public void Post([FromBody] Historico historico)
        {
            _historicoManager.GuardarHistorico(historico);
        }
    }
}
