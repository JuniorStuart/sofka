﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SofkaPreguntas.Managers;
using SofkaPreguntas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SofkaPreguntas.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PreguntasController : ControllerBase
    {
        private readonly PreguntasManager _preguntasManager;
        public PreguntasController(PreguntasManager preguntasManager)
        {
            _preguntasManager = preguntasManager;
        }

        [HttpGet("categorias/{nivel}")]
        public Pregunta Get(int nivel)
        {
            return _preguntasManager.PreguntaRandom(nivel);
        }
    }
}
