﻿using SofkaPreguntas.Models;
using SofkaPreguntas.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SofkaPreguntas.Managers
{
    public class PreguntasManager
    {
        private readonly PreguntasRepository _preguntasRepository;
        public PreguntasManager(PreguntasRepository preguntasRepository)
        {
            _preguntasRepository = preguntasRepository;
        }

        public Pregunta PreguntaRandom(int nivel)
        {
            var preguntas = _preguntasRepository.PreguntasPorNivel(nivel);
            var random = new Random();
            var pregunta = preguntas.ElementAt(random.Next(preguntas.Count()));
            pregunta.Respuestas = _preguntasRepository.RespuestasPorPregunta(pregunta.Id);
            return pregunta;
        }
    }
}
