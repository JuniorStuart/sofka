﻿using SofkaPreguntas.Models;
using SofkaPreguntas.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SofkaPreguntas.Managers
{
    public class HistoricoManager
    {
        private readonly HistoricoRepository _historicoRepository;
        public HistoricoManager(HistoricoRepository historicoRepository)
        {
            _historicoRepository = historicoRepository;
        }

        public IEnumerable<Historico> ObtenerHistorico()
        {
            return _historicoRepository.ObtenerHistorico();
        }

        public void GuardarHistorico(Historico historico)
        {
            _historicoRepository.GuardarResultado(historico);
        }
    }
}
