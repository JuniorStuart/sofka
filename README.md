# Sofka
Es una pequena aplicacion que permite jugar a preguntas y respuestas


## Ejecucion

- Antes de ejecutar la aplicacion se require restaurar la base de datos. Para esto luego de clonar el proyecto se requiere ejecutar el script de MS SQL Server en la carpeta Scripts.

- Una vez se haya restaurado de base de datos se requiere modificar la cadena de conexion en el archivo /Backend/SofkaPreguntas/appsettings.json para que apunte al servidor en el que se ejecutaron los scripts en el paso anterior.

- Luego de esto se debe ejecutar el proyecto en Visual Studio 2019. Se requiere tener dotnet core 3.1 SDK instalado.

- Validar que la aplication se ejecute en la URL https://localhost:5001

- Una vez la aplicacion de servidor este corriendo basta con abrir el archivo Nombre.html para empezar a jugar.